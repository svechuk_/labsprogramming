﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string firstname, secondname, thirdname, klass;
            int questions;
            bool complexity;
            if ((textBox1.TextLength == 0) || (textBox2.TextLength == 0) || (textBox3.TextLength == 0) || (numericUpDown1.Value < 0) || (comboBox1.SelectedIndex == -1))
            { Console.Beep(); MessageBox.Show("Введите все поля!"); }
            else
            {
                firstname = textBox2.Text;
                secondname = textBox1.Text;
                thirdname = textBox3.Text;
                questions = (int)numericUpDown1.Value;
                klass = comboBox1.Text;
                if (radioButton1.TabStop) complexity = false;
                else complexity = true;
                Form2 test = new Form2(firstname, secondname, thirdname, complexity, questions, klass);
                test.Show();
            }
        }
    }
}
