﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _1
{
    public partial class Form2 : Form
    {
        public Form2(string firstname, string secondname, string thirdname, bool complexity, int questions, string klass)
        {
            int i, x, y;
            Random random = new Random();
            InitializeComponent();
            label1.Text = "Ученик: " + firstname + "    " + secondname + "    " + thirdname + "    " + klass + " класс";
            label1.AutoSize = true;
            x = 10;
            y = 10;
            label1.Location = new Point(x, y);

            x = 20;
            if (questions == 0)
            {
                Label a = new Label();
                a.Text = "Вопросов нет";
                a.AutoSize = true;
                y = 50;
                a.Location = new Point(x, y);
                this.Controls.Add(a);
            }
            else
            {
                if (complexity)
                {
                    for (i = 0; i < questions; i++)
                    {
                        Label a = new Label();
                        a.Text = "Упражнение " + (i+1);
                        a.AutoSize = true;
                        y = (i + 1) * 50;
                        a.Location = new Point(x, y);
                        this.Controls.Add(a);
                        ComboBox b = new ComboBox();
                        b.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                        b.Items.AddRange(new object[] { random.Next(1, 100), random.Next(1, 100), random.Next(1, 100) });
                        b.Location = new Point(x + 150, y);
                        this.Controls.Add(b);
                    }
                }
                else
                {
                    for (i = 0; i < questions; i++)
                    {
                        Label a = new Label();
                        a.Text = "Вопрос " + (i+1);
                        a.AutoSize = true;
                        y = (i+1) * 50;
                        a.Location = new Point(x, y);
                        this.Controls.Add(a);
                        CheckBox b = new CheckBox();
                        b.Location = new Point(x + 60, y-5);
                        this.Controls.Add(b);
                    }
                }
            }
        }
    }
}
